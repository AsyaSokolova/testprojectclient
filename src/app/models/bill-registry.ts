export interface BillRegistryModel{
    ID: string;
    client: number;
    money: string;
}

export interface NewBillRegistryModel{
    client: number;
    money: string;
}

export interface BillChangeMoney{
    billFrom: string;
    billTo: string;
    assignment: string;
    money: string;
}
