export interface TransactionModel{
    ID: number;
    billFrom: string;
    billTo: string;
    amount: string;
}

export interface NewTransactionModel{
    billFrom: string;
    billTo: string;
    amount: string;
    assignment: string;
}
