export interface ClientRegistryModel{
    ID: number;
    name: string;
    age: number;
}

export interface NewClientRegistryModel{
    name: string;
    age: number;
}

