import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClentRegistryComponent } from './components/clent-registry.component';

const routes: Routes = [
    {
        path: '',
        component: ClentRegistryComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ClentRegistryRoutingModule {

}
