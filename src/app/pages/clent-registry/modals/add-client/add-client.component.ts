import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { NewClientRegistryModel } from 'src/app/models/client-registry';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {

  form: FormGroup;
  get result(): Subject<{ result: string; clientModel: NewClientRegistryModel }> {
    return this.resultModal;
  }

  set result(value: Subject<{ result: string; clientModel: NewClientRegistryModel }>) {
    this.resultModal = value;
  }

  resultModal: Subject<{ result: string; clientModel: NewClientRegistryModel }> = new Subject();

  constructor(
    private bsModalRef: BsModalRef,
    private fb: FormBuilder,
  ) {

    this.form = this.fb.group({
      name: new FormControl('', Validators.required),
      age: ''
    });
  }

  ngOnInit(): void {

  }

  onSubmit(result = 'save'): void {
    const clientModel: NewClientRegistryModel = {
      name: this.form.controls.name.value,
      age: this.form.controls.age.value,
    };

    this.resultModal.next({ result, clientModel });

    this.bsModalRef.hide();
  }

  close(): void {
    this.bsModalRef.hide();
  }

}
