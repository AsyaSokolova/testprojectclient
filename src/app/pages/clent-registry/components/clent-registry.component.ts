import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ClientRegistryModel, NewClientRegistryModel } from 'src/app/models/client-registry';
import { ClientRegistryService } from '../client-registry.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AddClientComponent } from '../modals/add-client/add-client.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-clent-registry',
  templateUrl: './clent-registry.component.html',
  styleUrls: ['./clent-registry.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ClentRegistryComponent implements OnInit {
  clients: ClientRegistryModel[] = [];
  mySelection: any;

  constructor(
    private clientRegistryService: ClientRegistryService,
    private bsModalService: BsModalService,
    private router: Router,
  ) { }

  ngOnInit(): void {

    this.getClients();
  }

  getClients(): void {
    this.clientRegistryService.getClients().subscribe((clients: ClientRegistryModel[]) => {
      this.clients = clients;
    }, error => {
      alert('Ошибка сервера!' + error);
    });
  }

  addClient(): void {
    const modal = this.bsModalService.show(AddClientComponent, {
      class: 'modal-big modal-dialog-centered',
      initialState: {},
      ignoreBackdropClick: true
    });

    modal.content.result.subscribe((data: { result: string, clientModel: NewClientRegistryModel }) => {
      this.saveClient(data.clientModel);
    });
  }

  saveClient(clientModel: NewClientRegistryModel): void {
    this.clientRegistryService.addClient(clientModel).subscribe((result: number) => {
      this.getClients();
    }, error => {
      alert('Ошибка сервера!' + error);
    });
  }

  onRowSelect(client: ClientRegistryModel): void {
    if (client.ID) {
      this.router.navigate(['/bills', { clientId: client.ID }]);
    }
  }

}
