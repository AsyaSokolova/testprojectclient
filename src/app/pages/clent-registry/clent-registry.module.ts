import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClentRegistryComponent } from './components/clent-registry.component';
import { ClentRegistryRoutingModule } from './client-registry.routing';
import { ClientRegistryService } from './client-registry.service';
import { TableModule } from 'primeng/table';
import { AddClientComponent } from './modals/add-client/add-client.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ModalModule, ModalBackdropComponent } from 'ngx-bootstrap/modal';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ClentRegistryComponent,
    AddClientComponent,
  ],
  imports: [
    CommonModule,
    TableModule,
    ClentRegistryRoutingModule,
    ModalModule.forRoot(),
    ReactiveFormsModule
  ],
  providers: [
    ClientRegistryService,
    BsModalService,
    ModalBackdropComponent
  ]
})
export class ClentRegistryModule { }
