import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ClientRegistryModel, NewClientRegistryModel } from 'src/app/models/client-registry';

/**
 * Service for API-call of operations with Company
 */
@Injectable()
export class ClientRegistryService {
    url = 'http://localhost:8080/bank/';

    constructor(private http: HttpClient) {
    }

    getClients(): Observable<ClientRegistryModel[]> {
        return this.http.get<ClientRegistryModel[]>(this.url + 'clients');
    }

    getClientById(id: number): Observable<ClientRegistryModel> {
        return this.http.get<ClientRegistryModel>(this.url + 'client/' + id);
    }

    addClient(clientModel: NewClientRegistryModel): Observable<number> {
        return this.http.post<number>(this.url + 'client', clientModel);
    }

    editClient(clientModel: ClientRegistryModel): Observable<number> {
        return this.http.put<number>(this.url + 'client', clientModel);
    }
}
