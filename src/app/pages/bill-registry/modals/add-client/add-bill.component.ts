import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { NewBillRegistryModel } from 'src/app/models/bill-registry';
import { ClientRegistryModel } from 'src/app/models/client-registry';

@Component({
  selector: 'app-add-bill',
  templateUrl: './add-bill.component.html',
  styleUrls: ['./add-bill.component.css']
})
export class AddBillComponent implements OnInit {

  client: any;
  form: FormGroup;
  get result(): Subject<{ result: string; billModel: NewBillRegistryModel }> {
    return this.resultModal;
  }

  set result(value: Subject<{ result: string; billModel: NewBillRegistryModel }>) {
    this.resultModal = value;
  }

  resultModal: Subject<{ result: string; billModel: NewBillRegistryModel }> = new Subject();

  constructor(
    private bsModalRef: BsModalRef,
    private fb: FormBuilder,
  ) {
    this.form = this.fb.group({
      money: ''
    });
  }

  ngOnInit(): void {

  }

  onSubmit(result = 'save'): void {

    const billModel: NewBillRegistryModel = {
      client: this.client.ID,
      money: this.form.controls.money.value,
    };

    this.resultModal.next({ result, billModel });
    this.bsModalRef.hide();
  }

  close(): void {
    this.bsModalRef.hide();
  }

}
