import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillRegistryComponent } from './component/bill-registry.component';

const routes: Routes = [
    {
        path: '',
        component: BillRegistryComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BillRegistryRoutingModule {

}
