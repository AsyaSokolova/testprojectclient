import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BillRegistryModel, NewBillRegistryModel } from 'src/app/models/bill-registry';
import { ClientRegistryModel } from 'src/app/models/client-registry';
import { ClientRegistryService } from '../../clent-registry/client-registry.service';
import { BillRegistryService } from '../bill-registry.service';
import { AddBillComponent } from '../modals/add-client/add-bill.component';

@Component({
  selector: 'app-bill-registry',
  templateUrl: './bill-registry.component.html',
  styleUrls: ['./bill-registry.component.css']
})
export class BillRegistryComponent implements OnInit {

  bills: BillRegistryModel[] = [];
  clientId: number;
  client: ClientRegistryModel | undefined;

  constructor(
    private billRegistryService: BillRegistryService,
    private activatedRoute: ActivatedRoute,
    private bsModalService: BsModalService,
    private clientRegistryService: ClientRegistryService
  ) {
    this.clientId = Number(this.activatedRoute.snapshot.paramMap.get('clientId'));
  }

  ngOnInit(): void {
    this.getBillsByClient();
    this.getClientById();
  }

  getBillsByClient(): void {

    this.billRegistryService.getBillsByClient(this.clientId).subscribe((bills: BillRegistryModel[]) => {
      this.bills = bills;
    }, error => {
      alert('Ошибка сервера!' + error);
    });
  }

  getClientById(): void {
    this.clientRegistryService.getClientById(this.clientId).subscribe((client: ClientRegistryModel) => {
      this.client = client;
    }, error => {
      alert('Ошибка сервера!' + error);
    });
  }

  addBill(): void {
    const client = this.client ;
    const modal = this.bsModalService.show(AddBillComponent, {
      class: 'modal-big modal-dialog-centered',
      initialState: { client },
      ignoreBackdropClick: true
    });

    modal.content.result.subscribe((data: { result: string, billModel: NewBillRegistryModel }) => {
      this.saveBill(data.billModel);
    });
  }

  saveBill(billModel: NewBillRegistryModel): void {
    this.billRegistryService.addBill(billModel).subscribe((result: number) => {
      this.getBillsByClient();
    }, error => {
      alert('Ошибка сервера!' + error);
    });
  }

}
