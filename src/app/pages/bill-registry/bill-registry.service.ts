import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { BillRegistryModel, NewBillRegistryModel } from 'src/app/models/bill-registry';

@Injectable({
  providedIn: 'root'
})
export class BillRegistryService {
  url = 'http://localhost:8080/bank/';

  constructor(private http: HttpClient) { }

  getBillsByClient(idClient: number): Observable<BillRegistryModel[]> {
    return this.http.get<BillRegistryModel[]>(this.url + 'bills/' + idClient);
  }

  addBill(billModel: NewBillRegistryModel): Observable<number> {
    return this.http.post<number>(this.url + 'bill', billModel);
  }

  getBills(): Observable<BillRegistryModel[]> {
    return this.http.get<BillRegistryModel[]>(this.url + 'bills');
  }

  editBill(model: any): Observable<number[]> {
    return this.http.put<number[]>(this.url + 'bill', model);
  }

}
