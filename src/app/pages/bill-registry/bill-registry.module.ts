import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BillRegistryComponent } from './component/bill-registry.component';
import { BillRegistryRoutingModule } from './bill-registry.routing';
import { BillRegistryService } from './bill-registry.service';
import { TableModule } from 'primeng/table';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ModalModule, ModalBackdropComponent } from 'ngx-bootstrap/modal';
import { ReactiveFormsModule } from '@angular/forms';
import { ClientRegistryService } from '../clent-registry/client-registry.service';
import { AddBillComponent } from './modals/add-client/add-bill.component';

@NgModule({
  declarations: [
    BillRegistryComponent,
    AddBillComponent,
  ],
  imports: [
    CommonModule,
    TableModule,
    BillRegistryRoutingModule,
    ModalModule.forRoot(),
    ReactiveFormsModule
  ],
  providers: [
    BillRegistryService,
    ClientRegistryService,
    BsModalService,
    ModalBackdropComponent
  ]
})
export class BillRegistryModule { }
