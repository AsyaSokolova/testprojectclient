import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionComponent } from './components/transaction.component';
import { TransactionRoutingModule } from './transaction.routing';
import { TransactionService } from './transaction.service';
import { TableModule } from 'primeng/table';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ModalModule, ModalBackdropComponent } from 'ngx-bootstrap/modal';
import { ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { AddTransactionComponent } from './modals/add-transaction/add-transaction.component';
import { ClientRegistryService } from '../clent-registry/client-registry.service';

@NgModule({
  declarations: [
    TransactionComponent,
    AddTransactionComponent
  ],
  imports: [
    CommonModule,
    TableModule,
    DropdownModule,
    TransactionRoutingModule,
    ModalModule.forRoot(),
    ReactiveFormsModule
  ],
  providers: [
    TransactionService,
    ClientRegistryService,
    BsModalService,
    ModalBackdropComponent,
    AddTransactionComponent
  ]
})
export class TransactionModule { }
