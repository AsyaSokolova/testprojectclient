import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormBuilder, NgForm, FormGroup, Validators, FormControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { BillRegistryModel } from 'src/app/models/bill-registry';
import { ClientRegistryModel } from 'src/app/models/client-registry';
import { NewTransactionModel } from 'src/app/models/transaction';

@Component({
  selector: 'app-add-transaction',
  templateUrl: './add-transaction.component.html',
  styleUrls: ['./add-transaction.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class AddTransactionComponent implements OnInit {

  form: FormGroup;
  bills: BillRegistryModel[] = [];
  clients: ClientRegistryModel[] = [];
  clientBillsFrom: BillRegistryModel[] = [];
  clientBillsTo: BillRegistryModel[] = [];

  errorValidateForm = false;
  errorValidateSum = false;
  assignments = [{
    id: 1,
    name: 'Снятие денег'
  },
  {
    id: 2,
    name: 'Покупка'
  },
  {
    id: 3,
    name: 'Пополнение баланса'
  },
  {
    id: 4,
    name: 'Перевод между счетами'
  }];

  selectedBill: any;

  get result(): Subject<{ result: string; transactionModel: any }> {
    return this.resultModal;
  }

  set result(value: Subject<{ result: string; transactionModel: any }>) {
    this.resultModal = value;
  }

  resultModal: Subject<{ result: string; transactionModel: any }> = new Subject();

  constructor(
    private bsModalRef: BsModalRef,
    private fb: FormBuilder,
  ) {

    this.form = this.initForm();
  }

  ngOnInit(): void {
    this.form = this.initForm();
    this.changeClient(this.clients[0].ID);
  }

  initForm(): FormGroup {
    return this.fb.group({
      client: new FormControl(this.clients.length > 0 ? this.clients[0] : ''),
      billTo: new FormControl(''),
      billFrom: new FormControl(''),
      amount: new FormControl(''),
      assignment: new FormControl(this.assignments[0]),
    });
  }

  onSubmit(result = 'save'): void {
    if (this.checkValidateForm() && this.checkAmount()) {
      const transactionModel = {
        billTo: this.form.get('assignment')?.value.id === 4 ? this.form.get('billTo')?.value?.ID : -1,
        billFrom: this.form.get('billFrom')?.value.ID,
        amount: this.form.get('amount')?.value,
        assignment: this.form.get('assignment')?.value.name
      };

      this.resultModal.next({ result, transactionModel });

      this.bsModalRef.hide();
    } else if (!this.checkValidateForm()) {
      this.errorValidateForm = true;
    } else if (!this.checkAmount()) {
      this.errorValidateSum = true;
    }

  }

  checkValidateForm(): boolean {
    if (this.form.get('assignment')?.value === 4) {

      return this.form.get('client')?.value &&
        this.form.get('billTo')?.value &&
        this.form.get('amount')?.value &&
        this.form.get('billFrom')?.value;
    } else {
      return this.form.get('client')?.value &&
        this.form.get('amount')?.value &&
        this.form.get('assignment')?.value &&
        this.form.get('billFrom')?.value;
    }
  }

  checkAmount(): boolean {
    return this.form.get('amount')?.value <= this.form.get('billFrom')?.value?.money;
  }

  close(): void {
    this.bsModalRef.hide();
  }

  changeClient(event: number): void {
    this.clientBillsFrom = [];

    this.bills.map(bill => {
      if (bill.client === event) {
        this.clientBillsFrom.push(bill);
      }
    });
    this.form.get('billFrom')?.setValue(this.clientBillsFrom.length > 0 ? this.clientBillsFrom[0] : '');
    this.changeBillsFrom(this.clientBillsFrom[0]);
  }

  changeBillsFrom(event: BillRegistryModel): void {
    this.clientBillsTo = this.clientBillsFrom.filter((bill: any) => {
      if (bill.ID !== event.ID) {
        return bill;
      }
    });
    this.form.get('billTo')?.setValue(this.clientBillsTo.length > 0 ? this.clientBillsTo[0] : '');
  }

}
