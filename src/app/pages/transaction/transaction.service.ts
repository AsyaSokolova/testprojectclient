import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NewTransactionModel, TransactionModel } from 'src/app/models/transaction';

/**
 * Service for API-call of operations with Company
 */
@Injectable()
export class TransactionService {
    url = 'http://localhost:8080/bank/';

    constructor(private http: HttpClient) {
    }

    getTransactions(): Observable<TransactionModel[]> {
        return this.http.get<TransactionModel[]>(this.url + 'transactions');
    }

    addTransaction(transactionModel: NewTransactionModel): Observable<number> {
        return this.http.post<number>(this.url + 'transaction', transactionModel);
    }

}
