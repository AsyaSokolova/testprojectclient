import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BillChangeMoney, BillRegistryModel } from 'src/app/models/bill-registry';
import { ClientRegistryModel } from 'src/app/models/client-registry';
import { NewTransactionModel, TransactionModel } from 'src/app/models/transaction';
import { BillRegistryService } from '../../bill-registry/bill-registry.service';
import { ClientRegistryService } from '../../clent-registry/client-registry.service';
import { AddTransactionComponent } from '../modals/add-transaction/add-transaction.component';
import { TransactionService } from '../transaction.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class TransactionComponent implements OnInit {

  transactions: TransactionModel[] = [];
  bills: BillRegistryModel[] = [];
  clients: ClientRegistryModel[] = [];
  constructor(
    private transactionService: TransactionService,
    private bsModalService: BsModalService,
    private billRegistryService: BillRegistryService,
    private clientRegistryService: ClientRegistryService
  ) { }

  ngOnInit(): void {
    this.getTransactions();
    this.getBills();
  }

  getTransactions(): void {
    this.transactionService.getTransactions().subscribe((transactions: TransactionModel[]) => {
      this.transactions = transactions;
    }, error => {
      alert('Ошибка сервера!' + error);
    });
  }

  getBills(): void {
    this.billRegistryService.getBills().subscribe((bills: BillRegistryModel[]) => {
      this.bills = bills;
      this.getClients();
    }, error => {
      alert('Ошибка сервера!' + error);
    });
  }

  getClients(): void {
    this.clientRegistryService.getClients().subscribe((clients: ClientRegistryModel[]) => {
      this.clients = clients;
    }, error => {
      alert('Ошибка сервера!' + error);
    });
  }

  addTransaction(): void {
    const bills = this.bills;
    const clients = this.clients;

    const modal = this.bsModalService.show(AddTransactionComponent, {
      class: 'modal-big modal-dialog-centered',
      initialState: { bills, clients },
      ignoreBackdropClick: true
    });

    modal.content.result.subscribe((data: { result: string, transactionModel: any }) => {
      this.saveTransaction(data.transactionModel);
    });

  }

  saveTransaction(transactionModel: NewTransactionModel): void {
    this.transactionService.addTransaction(transactionModel).subscribe((result) => {
      this.getTransactions();
      const model: BillChangeMoney = {
        billFrom: transactionModel.billFrom,
        billTo: transactionModel.billTo,
        money: transactionModel.amount,
        assignment: transactionModel.assignment
      };
      this.editBill(model);
    }, error => {
      alert('Ошибка сервера!' + error);
    });
  }

  editBill(model: BillChangeMoney): void {
    this.billRegistryService.editBill(model).subscribe((result) => {
      this.getBills();
    }, error => {
      alert('Ошибка сервера!' + error);
    });
  }

}
