import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: 'clients',
    loadChildren: () => import('./pages/clent-registry/clent-registry.module').then(m => m.ClentRegistryModule)
  },
  {
    path: 'bills',
    loadChildren: () => import('./pages/bill-registry/bill-registry.module').then(m => m.BillRegistryModule)
  },
  {
    path: 'transactions',
    loadChildren: () => import('./pages/transaction/transaction.module').then(m => m.TransactionModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
